app.controller('userCreateCtrl', function($location, $scope, $http, $resource, Upload) {
    var User = $resource('/api/v1/user/:userId', {userId:'@id'});

    $scope.user = new User({});

    // $scope.user = User.get({userId:1}, function(data) {
        // user.abc = true;
        // user.$save();
    // });


    $scope.userSaveLoader = false;
    $scope.create = function() {
        $scope.userSaveLoader = true;
        $scope.user.$save(function() {
            $.notify({
                // title: '<strong></strong>',
                message: 'User has been created!'
            },{
                type: 'success'
            });
            $scope.userSaveLoader = false;
            $location.path( "/users/list" );
        }, function(param) {
            $.notify({
                title: '<strong>Error!</strong>',
                message: 'Cannot save user.'
            },{
                type: 'danger'
            });
            $scope.userSaveLoader = false;
        });
    };

    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: '/admin/upload/gov_id',
            data: {file: file}
        }).then(function (response) {
            $.notify({
                message: 'File has been uploaded!'
            },{
                type: 'success'
            });
            $scope.user.govId = response.data.url;
        }, function (resp) {
            $.notify({
                title: '<strong>Error!</strong>',
                message: 'Cannot upload the file.'
            },{
                type: 'danger'
            });
        }/*, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }*/);
    };

    $scope.crop = function (file) {
        Upload.base64DataUrl(file).then(function(url){
            $scope.myImage=url;
        })
    }


});

app.controller('userEditCtrl', function($location, $scope, $routeParams, $resource, $http) {
    var User = $resource('/api/v1/user/:userId', {userId:'@id'});
    $scope.userSaveLoader = false;

    $scope.user = User.get({userId:$routeParams.userId}, function() {
        $scope.initial = {
            username: $scope.user.username,
            email: $scope.user.email
        };
        $scope.save = function() {
            $scope.userSaveLoader = true;
            $scope.user.$save(function() {
                $.notify({
                    message: 'User has been updated!'
                },{
                    type: 'success'
                });
                $scope.userSaveLoader = false;
                $location.path( "/users/list" );
            }, function(param) {
                $.notify({
                    title: '<strong>Error!</strong>',
                    message: 'Cannot save user.'
                },{
                    type: 'danger'
                });
                $scope.userSaveLoader = false;
            });
        };
        $scope.toggleActivate = function() {
            $scope.user.enabled = !$scope.user.enabled;
            $scope.userActivateLoader = true;
            $scope.user.$save(function() {
                $.notify({
                    message: 'User has been '+($scope.user.enabled ? 'activated' : 'deactivated')+'!'
                },{
                    type: 'success'
                });
                $scope.userActivateLoader = false;
            }, function(param) {
                $.notify({
                    title: '<strong>Error!</strong>',
                    message: 'Cannot save user.'
                },{
                    type: 'danger'
                });
                $scope.user.enabled = !$scope.user.enabled;
                $scope.userActivateLoader = false;
            });
        }
    });

    $scope.openDeleteAlert = function() {
        swal({
                title: "Are you sure?",
                text: "User will be completely deleted!",
                type: "error",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Remove",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            },
            function(){
                $scope.user.$delete(function() {
                    swal({
                        title: "Deleted!",
                        text: "User has been deleted",
                        type: "success",
                        confirmButtonClass: "btn-success"
                    });
                    $location.path( "/users/list" );
                }, function(param) {
                    $.notify({
                        title: '<strong>Error!</strong>',
                        message: 'Cannot delete user.'
                    },{
                        type: 'danger'
                    });
                    $scope.userSaveLoader = false;
                });


            });
    }

    $scope.uploadFile = function(files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);

        $http.post('/admin/upload/gov_id', fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).then(function(response) {
            $.notify({
                message: 'File has been uploaded!'
            },{
                type: 'success'
            });
            $scope.user.govId = response.data.url;

        }, function(param) {
            $.notify({
                title: '<strong>Error!</strong>',
                message: 'Cannot upload the file.'
            },{
                type: 'danger'
            });
        });
    };

    $scope.myImage='';
    $scope.user.picture='';

    var handleFileSelect=function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.myImage=evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#user-create-form-picture')).on('change',handleFileSelect);
});

app.controller('userListCtrl', function($location, $scope, $routeParams, $resource) {
    var User = $resource('/api/v1/user/:userId', {userId:'@id'});

    $scope.getRole = function(role) {
        var roles = {
            'ROLE_ADMIN': 'Admin',
            'ROLE_MANAGER': 'Manager',
            'ROLE_TECH': 'Technician'
        };
        if(Object.keys(roles).indexOf(role) === -1) {
            console.log('Undefined role');
            return '';
        } else {
            return roles[role]
        }
    };

    $scope.users = User.query();
});

app.directive('checkAvailability', function ($http, $q, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            var name = attr.name;

            ngModel.$asyncValidators.invalidUsername = function(modelValue, viewValue) {
                var deferred = $q.defer();

                // ask the server if this username exists


                if(angular.isObject(scope.initial) && scope.initial[name] === viewValue) {
                    deferred.resolve();
                }
                scope.checkingUsername = true;
                $http.get('/user-exists', {params: {field: name, value: viewValue} }).then(
                    function(response) {
                        scope.checkingUsername = false;
                        if (response.data.exists) {
                            deferred.reject();
                        } else {
                            deferred.resolve();
                        }

                    }, function() {
                        scope.checkingUsername = false;
                    });

                // return the promise of the asynchronous validator
                return deferred.promise;
            }
        }
    }
});