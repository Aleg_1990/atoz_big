app.controller('orderCreateCtrl', function($location, $scope, $http, $resource) {
    var WorkOrder = $resource('/api/v1/work-order/:orderId', {orderId:'@id'});

    $scope.order = new WorkOrder({
        appliances: [{}]
    });

    $scope.removeAppliance = function(appliance) {
        var index = $scope.order.appliances.indexOf(appliance);
        $scope.order.appliances.splice(index, 1);
    };

    $scope.pickerIcons = {
            time: "icmn-clock",
            date: "icmn-calendar2",
            up: "icmn-arrow-up2",
            down: "icmn-arrow-down"
    };
    $scope.date = new Date();

    $scope.orderSaveLoader = false;
    $scope.create = function() {
        $scope.orderSaveLoader = true;
        $scope.order.$save(function() {
            $.notify({
                // title: '<strong></strong>',
                message: 'Order has been created!'
            },{
                type: 'success'
            });
            $scope.orderSaveLoader = false;
            $location.path( "/" );
        }, function(param) {
            $.notify({
                title: '<strong>Error!</strong>',
                message: 'Cannot save order.'
            },{
                type: 'danger'
            });
            $scope.orderSaveLoader = false;
        });
    };
});

app.controller('orderViewCtrl', function($location, $scope, $routeParams, $resource, $http, $filter) {
    var User = $resource('/api/v1/work-order/:orderId', {orderId:'@id'});
    $scope.order = User.get({orderId:$routeParams.orderId}, function() {
/*        $scope.order.appliances = [
            {
                id: 1,
                type: 'Refrigerator',
                brand: 'BOSCH',
                complaint: 'Not cooling'
            }
        ];*/
        $scope.save = function() {
            $scope.order.$save(function() {
                // success
            }, function(param) {
                $.notify({
                    title: '<strong>Error!</strong>',
                    message: 'Cannot save order.'
                },{
                    type: 'danger'
                });
            });
        };
        $scope.addAppliance = function() {
            $scope.order.appliances.push(angular.copy($scope.newAppliance));
            $scope.newAppliance = false;
            $scope.save()
        };
    });

    $scope.getAppointmentDate = function() {
        return moment($scope.appointment_date).format('L');
    }

    $scope.showStatus = function() {
        var selected = $filter('filter')($scope.workOrderStatuses, {id: $scope.order.status});
        return ($scope.order.status && selected.length) ? selected[0].name : 'Not set';
    };
});