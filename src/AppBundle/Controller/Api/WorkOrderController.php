<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\WorkOrder;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class WorkOrderController extends FOSRestController
{
    /**
     * @Rest\Post("api/v1/work-order")
     */
    public function postAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $this->get('serializer')
            ->deserialize(
                $request->getContent(),
                WorkOrder::class,
                'json'
            );

        $em->persist($order);
        $em->flush();

        return new View("Work order successfully updated", Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get("/api/v1/work-order/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:WorkOrder')->find($id);
        if ($singleresult === null) {
            return new View("Order not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("api/v1/work-order/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $workOrder = $em->getRepository('AppBundle:WorkOrder')->find($id);

        $this->get('serializer')
            ->deserialize(
                $request->getContent(),
                WorkOrder::class,
                'json',
                ['object_to_populate' => $workOrder]
            );
        $em->flush();

        return $workOrder;
    }
}
