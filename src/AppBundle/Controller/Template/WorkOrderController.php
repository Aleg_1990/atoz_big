<?php

namespace AppBundle\Controller\Template;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class WorkOrderController/* extends Controller*/
{
    /**
     * @Route("/work-order/create.html", name="admin_work_order_create")
     * @Template()
     */
    public function createAction()
    {
        return [];
    }
    /**
     * @Route("/work-order/view.html", name="admin_work_order_view")
     * @Template()
     */
    public function viewAction()
    {
        return [];
    }
}
