<?php

namespace AppBundle\Controller\Template;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     * @Template()
     */
    public function dashboardAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/dashboards/alpha.html", name="admin_dashboard")
     * @Template()
     */
    public function dashboardAlphaAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/admin/upload/gov_id", name="admin_upload_gov_id")
     */
    public function uploadGovIdAction(Request $request)
    {
        /**
         * @var $file UploadedFile
         */
        $file = $request->files->get('file');

        $file->move(
            $this->getParameter('kernel.root_dir').'/../web/uploads/gov_ids/',
            $file->getClientOriginalName()
        );

        return $this->json([
            'result' => 'success',
            'url' => '/uploads/gov_ids/'.$file->getClientOriginalName(),
            'filename' => $file->getClientOriginalName()
        ]);
    }
}
