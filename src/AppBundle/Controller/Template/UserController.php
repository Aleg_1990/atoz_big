<?php

namespace AppBundle\Controller\Template;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends Controller
{
    /**
     * @Route("/users/create.html", name="admin_users_create")
     * @Template()
     */
    public function createAction()
    {
        return [];
    }

    /**
     * @Route("/users/edit.html", name="admin_users_edit")
     * @Template()
     */
    public function editAction()
    {
        return [];
    }

    /**
     * @Route("/users/list.html", name="admin_users_list")
     * @Template()
     */
    public function listAction()
    {
        $users = $this->get('doctrine')->getEntityManager()->getRepository('AppBundle:User')->findAll();
        return [
            'users' => $users
        ];
    }

    /**
     * @Route("/user-exists", name="check_username_exists")
     */
    public function usernameExistsAction(Request $request)
    {
        $user = false;


        if($request->query->get('field') === 'username') {
            $username = $request->query->get('value');
            $user = $this->get('fos_user.user_manager')->findUserByUsername($username);
        } elseif ($request->query->get('field') === 'email') {
            $email = $request->query->get('value');
            $user = $this->get('fos_user.user_manager')->findUserByEmail($email);
        }
        return $this->json(['exists' => $user instanceof User]);
    }
}
