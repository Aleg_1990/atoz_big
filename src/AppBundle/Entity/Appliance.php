<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Appliance
 *
 * @ORM\Table(name="appliance")
 * @ORM\Entity()
 */
class Appliance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=100)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="complaint", type="text")
     */
    private $complaint;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\WorkOrder", inversedBy="appliances")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $workOrder;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Appliance
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Appliance
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set complaint
     *
     * @param string $complaint
     *
     * @return Appliance
     */
    public function setComplaint($complaint)
    {
        $this->complaint = $complaint;

        return $this;
    }

    /**
     * Get complaint
     *
     * @return string
     */
    public function getComplaint()
    {
        return $this->complaint;
    }

    /**
     * Set workOrder
     *
     * @param \AppBundle\Entity\WorkOrder $workOrder
     *
     * @return Appliance
     */
    public function setWorkOrder(\AppBundle\Entity\WorkOrder $workOrder = null)
    {
        $this->workOrder = $workOrder;

        return $this;
    }

    /**
     * Get workOrder
     *
     * @return \AppBundle\Entity\WorkOrder
     */
    public function getWorkOrder()
    {
        return $this->workOrder;
    }
}
