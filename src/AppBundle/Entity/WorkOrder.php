<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Transaction
 *
 * @ORM\Table(name="work_order")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class WorkOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $issuer;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $number;

    /**
     * @var WorkOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\WorkOrderStatus", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $appointmentDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $appointmentTimeFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $appointmentTimeTo;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinTable(name="work_order_assignee",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $assignees;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $collect;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $privateNotes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $customerName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     *
     * @Assert\NotBlank()
     */
    private $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Appliance", mappedBy="workOrder", cascade={"persist"})
     */
    private $appliances;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assignees =  new ArrayCollection();
        $this->appliances = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return WorkOrder
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkOrder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set issuer
     *
     * @param string $issuer
     *
     * @return WorkOrder
     */
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;

        return $this;
    }

    /**
     * Get issuer
     *
     * @return string
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return WorkOrder
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set appointmentDate
     *
     * @param \DateTime $appointmentDate
     *
     * @return WorkOrder
     */
    public function setAppointmentDate($appointmentDate)
    {
        $this->appointmentDate = $appointmentDate;

        return $this;
    }

    /**
     * Get appointmentDate
     *
     * @return \DateTime
     */
    public function getAppointmentDate()
    {
        return $this->appointmentDate;
    }

    /**
     * Set appointmentTimeFrom
     *
     * @param string $appointmentTimeFrom
     *
     * @return WorkOrder
     */
    public function setAppointmentTimeFrom($appointmentTimeFrom)
    {
        $this->appointmentTimeFrom = $appointmentTimeFrom;

        return $this;
    }

    /**
     * Get appointmentTimeFrom
     *
     * @return string
     */
    public function getAppointmentTimeFrom()
    {
        return $this->appointmentTimeFrom;
    }

    /**
     * Set appointmentTimeTo
     *
     * @param string $appointmentTimeTo
     *
     * @return WorkOrder
     */
    public function setAppointmentTimeTo($appointmentTimeTo)
    {
        $this->appointmentTimeTo = $appointmentTimeTo;

        return $this;
    }

    /**
     * Get appointmentTimeTo
     *
     * @return string
     */
    public function getAppointmentTimeTo()
    {
        return $this->appointmentTimeTo;
    }

    /**
     * Set collect
     *
     * @param string $collect
     *
     * @return WorkOrder
     */
    public function setCollect($collect)
    {
        $this->collect = $collect;

        return $this;
    }

    /**
     * Get collect
     *
     * @return string
     */
    public function getCollect()
    {
        return $this->collect;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return WorkOrder
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set privateNotes
     *
     * @param string $privateNotes
     *
     * @return WorkOrder
     */
    public function setPrivateNotes($privateNotes)
    {
        $this->privateNotes = $privateNotes;

        return $this;
    }

    /**
     * Get privateNotes
     *
     * @return string
     */
    public function getPrivateNotes()
    {
        return $this->privateNotes;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return WorkOrder
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return WorkOrder
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return WorkOrder
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return WorkOrder
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return WorkOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\WorkOrderStatus $status
     *
     * @return WorkOrder
     */
    public function setStatus(\AppBundle\Entity\WorkOrderStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\WorkOrderStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add assignee
     *
     * @param \AppBundle\Entity\User $assignee
     *
     * @return WorkOrder
     */
    public function addAssignee(\AppBundle\Entity\User $assignee)
    {
        $this->assignees[] = $assignee;

        return $this;
    }

    /**
     * Remove assignee
     *
     * @param \AppBundle\Entity\User $assignee
     */
    public function removeAssignee(\AppBundle\Entity\User $assignee)
    {
        $this->assignees->removeElement($assignee);
    }

    /**
     * Get assignees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignees()
    {
        return $this->assignees;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return WorkOrder
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add appliance
     *
     * @param \AppBundle\Entity\Appliance $appliance
     *
     * @return WorkOrder
     */
    public function addAppliance(\AppBundle\Entity\Appliance $appliance)
    {
        $this->appliances[] = $appliance;

        return $this;
    }

    /**
     * Remove appliance
     *
     * @param \AppBundle\Entity\Appliance $appliance
     */
    public function removeAppliance(\AppBundle\Entity\Appliance $appliance)
    {
        $this->appliances->removeElement($appliance);
    }

    /**
     * Get appliances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppliances()
    {
        return $this->appliances;
    }
}
