<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    const REGION_EAST_COAST = 'East coast';
    const REGION_WEST_COAST = 'West coast';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter first name.", groups={"Registration", "TechnicianEdit"})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter last name.", groups={"Registration", "TechnicianEdit"})
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter address.", groups={"Registration", "Profile"})
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter phone.", groups={"Registration", "Profile"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $region;

    /**
     * @ORM\Column(type="integer", nullable=true, length=2)
     */
    protected $skills;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sealedSystem;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $employmentType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $vehicle;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $govId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $picture;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * User construct
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return User
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set skills
     *
     * @param integer $skills
     *
     * @return User
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return integer
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set sealedSystem
     *
     * @param string $sealedSystem
     *
     * @return User
     */
    public function setSealedSystem($sealedSystem)
    {
        $this->sealedSystem = $sealedSystem;

        return $this;
    }

    /**
     * Get sealedSystem
     *
     * @return string
     */
    public function getSealedSystem()
    {
        return $this->sealedSystem;
    }

    /**
     * Set employmentType
     *
     * @param string $employmentType
     *
     * @return User
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;

        return $this;
    }

    /**
     * Get employmentType
     *
     * @return string
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * Set vehicle
     *
     * @param string $vehicle
     *
     * @return User
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set govId
     *
     * @param string $govId
     *
     * @return User
     */
    public function setGovId($govId)
    {
        $this->govId = $govId;

        return $this;
    }

    /**
     * Get govId
     *
     * @return string
     */
    public function getGovId()
    {
        return $this->govId;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return User
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }
}
