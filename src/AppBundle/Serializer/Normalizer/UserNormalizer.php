<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\User;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * User normalizer
 */
class UserNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($user, $format = null, array $context = array())
    {
        /**
         * @var $user User
         */
        return [
            'id'     => $user->getId(),
            'role'  => $user->getRoles()[0],
            'region' => $user->getRegion(),
            'username'   => $user->getUsername(),
            'firstname'  => $user->getFirstname(),
            'lastname'  => $user->getLastname(),
            'phone'  => $user->getPhone(),
            'email'  => $user->getEmail(),
            'address'  => $user->getAddress(),
            'vehicle'  => $user->getVehicle(),
            'skills'  => $user->getSkills() > 0 ? (string) $user->getSkills() : null,
            'sealed_system'  => $user->getSealedSystem(),
            'employment_type'  => $user->getEmploymentType(),
            'govId'  => $user->getGovId(),
            'picture'  => $user->getPicture(),
            'enabled' => $user->isEnabled()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof User;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $user =  isset($context['object_to_populate']) && $context['object_to_populate'] instanceof User ? $context['object_to_populate'] : new $class();

        @$user
            ->setRoles([$data['role']])
            ->setRegion($data['region'])
            ->setUsername($data['username'])
            ->setFirstname($data['firstname'])
            ->setLastname($data['lastname'])
            ->setPhone($data['phone'])
            ->setEmail($data['email'])
            ->setAddress($data['address'])
            ->setVehicle($data['vehicle'])
            ->setSkills($data['skills'])
            ->setSealedSystem($data['sealed_system'])
            ->setEmploymentType($data['employment_type'])
            ->setGovId($data['govId'])
            ->setPicture($data['picture'])
            ->setEnabled(isset($data['enabled']) ? $data['enabled'] : false)
        ;
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === User::class;
    }
}