<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Appliance;
use AppBundle\Entity\User;
use AppBundle\Entity\WorkOrder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

/**
 * Work order normalizer
 */
class WorkOrderNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerAwareTrait;
    use DenormalizerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * WorkOrderNormalizer constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($order, $format = null, array $context = array())
    {
        /**
         * @var $order WorkOrder
         */
        return [
            'id'     => $order->getId(),
            'region' => $order->getRegion(),
            'name' => $order->getName(),
            'issuer' => $order->getIssuer(),
            'number' => (float) $order->getNumber(),
            'status' => $order->getStatus()->getId(),
            'appointment_date' => $order->getAppointmentDate()->format(\DateTime::W3C),
            'appointment_time_from' => $order->getAppointmentTimeFrom(),
            'appointment_time_to' => $order->getAppointmentTimeTo(),
            'assignees' => $order->getAssignees()->map(
                    function($assignee) {
                        return (string) $assignee->getId();
                    }
                )->toArray(),
            'collect' => $order->getCollect(),
            'notes' => $order->getNotes(),
            'private_notes' => $order->getPrivateNotes(),

            'address' => $order->getAddress(),
            'customer_name' => $order->getCustomerName(),
            'phone' => $order->getPhone(),
            'email' => $order->getEmail(),
            'appliances' => $order->getAppliances()->map(function($appliance) use ($format, $context) {
                return $this->normalizer->normalize($appliance, $format, $context);
            })->toArray(),
            'recalls' => 0
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof WorkOrder;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        /**
         * @var $order WorkOrder
         */
        $order =  isset($context['object_to_populate']) && $context['object_to_populate'] instanceof WorkOrder ? $context['object_to_populate'] : new $class();

        if(!empty($data['region'])) {
            $order->setRegion($data['region']);
        }
        if(!empty($data['name'])) {
            $order->setName($data['name']);
        }
        if(!empty($data['issuer'])) {
            $order->setIssuer($data['issuer']);
        }
        if(!empty($data['number'])) {
            $order->setNumber($data['number']);
        }
        if(!empty($data['status'])) {
            $order->setStatus($this->em->getReference('AppBundle:WorkOrderStatus', $data['status']));
        }
        if(!empty($data['appointment_date'])) {
            $order->setAppointmentDate(new \DateTime($data['appointment_date']));
        }
        if(!empty($data['appointment_time_from'])) {
            $order->setAppointmentTimeFrom((new \DateTime($data['appointment_time_from']))->format('h:i A'));
        }
        if(!empty($data['appointment_time_to'])) {
            $order->setAppointmentTimeTo((new \DateTime($data['appointment_time_to']))->format('h:i A'));
        }
        if(!empty($data['assignees'])) {
            foreach ($order->getAssignees() as $assignee) {
                if(!isset($data['assignees'][$assignee->getId()])) {
                    $order->removeAssignee($assignee);
                }
            }
            foreach ($data['assignees'] as $assignee_id) {
                if(!$order->getAssignees()->contains($this->em->getReference(User::class, $assignee_id))) {
                    $order->addAssignee($this->em->getReference(User::class, $assignee_id));
                }
            }
        }
        if(!empty($data['collect'])) {
            $order->setCollect($data['collect']);
        }
        if(!empty($data['notes'])) {
            $order->setNotes($data['notes']);
        }
        if(!empty($data['private_notes'])) {
            $order->setPrivateNotes($data['private_notes']);
        }
        if(!empty($data['address'])) {
            $order->setAddress($data['address']);
        }
        if(!empty($data['customer_name'])) {
            $order->setCustomerName($data['customer_name']);
        }
        if(!empty($data['phone'])) {
            $order->setPhone($data['phone']);
        }
        if(!empty($data['email'])) {
            $order->setEmail($data['email']);
        }
        if(!empty($data['appliances'])) {
            foreach ($data['appliances'] as $appliance) {
                $applianceObj = $this->denormalizer->denormalize(
                    $appliance,
                    Appliance::class,
                    'json',
                    ['object_to_populate' => isset($appliance['id']) ? $this->em->getReference(Appliance::class, $appliance['id']) : null]
                );
                $applianceObj->setWorkOrder($order);
                $order->addAppliance($applianceObj);
            }
        }

        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === WorkOrder::class;
    }
}