<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Appliance;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Work order normalizer
 */
class ApplianceNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * WorkOrderNormalizer constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * {@inheritdoc}
     */
    public function normalize($appliance, $format = null, array $context = array())
    {
        /**
         * @var $appliance Appliance
         */
        return [
            'id'     => $appliance->getId(),
            'type'   => $appliance->getType(),
            'brand'  => $appliance->getBrand(),
            'complaint' => $appliance->getComplaint()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Appliance;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        /**
         * @var $appliance Appliance
         */
        $appliance =  isset($context['object_to_populate']) && $context['object_to_populate'] instanceof Appliance ? $context['object_to_populate'] : new $class();

        if(!empty($data['type'])) {
            $appliance->setType($data['type']);
        }
        if(!empty($data['brand'])) {
            $appliance->setBrand($data['brand']);
        }
        if(!empty($data['complaint'])) {
            $appliance->setComplaint($data['complaint']);
        }

        return $appliance;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === Appliance::class;
    }
}